import numpy as np
import matplotlib.pyplot as plt
from collections import deque

class Modelo_Ising(object):
    """
    Esta clase implementa el algoritmo de Metrópolis para el modelo de Ising.    
    """
    
    def __init__(self, L,iteraciones=1000):
        """
        Inicializa la clase con la longitud de la red y el número de iteraciones.
        
        Parámetros
        ----------
        L : int
            Longitud de la red cuadrada.
            
        iteraciones : int
            Número de iteraciones del algoritmo de Metrópolis.
        """
        self.L = L
        self.iteraciones = iteraciones
        self.puntos = 40
        self.Ts = np.linspace(1.0, 4, self.puntos)
        
        #manejo de errores
        if not isinstance(L, list):
            raise TypeError("L debe ser una lista")
        if not isinstance(iteraciones, int):
            raise TypeError("iteraciones debe ser un entero")
    
    def aplanar_2d(self, i, j, a):
        """
        Esta función aplana una matriz de dos dimensiones en un vector de una dimensión
        """
        return i * a + j  # Número de serie = Número de fila * longitud + Número de columna

    def desaplanar_2d(self, n, a):
        """
        Esta función desaplana un vector de una dimensión en una matriz de dos dimensiones
        """
        j = n % a
        i = (n - j) // a
        return i, j
    
    
    def gen_vecinos_2d(self, a):
        """
        Esta función genera una matriz de vecinos para una red cuadrada de longitud a.
        """
        vecinos = np.zeros((a * a, 4), dtype=int)
        for n in range(a * a):
            i, j = self.desaplanar_2d(n, a)
            vecinos[n][0] = self.aplanar_2d(i, (j - 1) % a, a)  # izquierda
            vecinos[n][1] = self.aplanar_2d(i, (j + 1) % a, a)  # derecha
            vecinos[n][2] = self.aplanar_2d((i - 1) % a, j, a)  # arriba
            vecinos[n][3] = self.aplanar_2d((i + 1) % a, j, a)  # abajo
        return vecinos
    
    
    def cluster_MC(self, lista_vecinos, T, iteraciones):
        """
        Esta función implementa el algoritmo de Metrópolis para el modelo de Ising.
        """
        p = 1 - np.exp(-2 / T)  # "número mágico"
        
        # Inicialización
        tamaño = lista_vecinos.shape[0]
        spins = np.random.randint(0, 2, tamaño)
        spins[spins == 0] = -1
        
        # Asignación
        magnetizacion = np.zeros(iteraciones + 1)
        magnetizacion[0] = spins.sum()
        energía = np.zeros(iteraciones + 1)
        energía[0] = -spins.dot(spins[lista_vecinos].sum(axis=1))
        
        for paso in range(iteraciones):
            # Usar deque para implementar la búsqueda en amplitud
            n0 = np.random.randint(0, tamaño)
            signo = spins[n0]
            cluster = set([n0])
            bolsillos = deque([n0])
            terminado = False
            while not terminado:
                try:
                    n = bolsillos.popleft()
                    vecinos = lista_vecinos[n]
                    for vecino in vecinos:
                        if spins[vecino] == signo and vecino not in cluster and np.random.rand() < p:
                            cluster.add(vecino)
                            bolsillos.append(vecino)
                except IndexError:
                    terminado = True
            # Voltear el cluster
            cluster = np.fromiter(cluster, dtype=int)
            spins[cluster] = -signo
            magnetizacion[paso + 1] = magnetizacion[paso] - 2 * signo * len(cluster)
            energía[paso + 1] = -spins.dot(spins[lista_vecinos].sum(axis=1))
        return magnetizacion / tamaño, energía / 2  # Cada par se cuenta dos veces

    def Magnetizacion_(self,L):
        """
        Esta función calcula la magnetización para una red cuadrada de longitud L.
        """
        puntos = self.puntos
        dimensión = L
        iteraciones = self.iteraciones
        
        lista_vecinos = self.gen_vecinos_2d(dimensión)
        
        Ts = np.linspace(1.0, 4.0, puntos)
        Ms = np.zeros(puntos)
        
        for i in range(puntos):
            T = Ts[i]
            magnetizacion, _ = self.cluster_MC(lista_vecinos, T, iteraciones)
            Ms[i] = np.abs(magnetizacion).mean()
        
        return Ts, Ms

    def Calor_Especifico(self,L):
        """
        Esta función calcula el calor específico para una red cuadrada de longitud L.
        """
        puntos = self.puntos
        dimensión = L
        iteraciones = self.iteraciones
        
        lista_vecinos = self.gen_vecinos_2d(dimensión)
        
        Ts = np.linspace(1.0, 4.0, puntos)
        CVs = np.zeros(puntos)
        
        for i in range(puntos):
            T = Ts[i]
            _, energía = self.cluster_MC(lista_vecinos, T, iteraciones)
            CVs[i] = energía.var() / T
            
        return Ts, CVs
        
    def Grafica_Magnetizacion(self):
        """
        Esta función grafica la magnetización para diferentes longitudes de red.
        """
        for i in self.L:
            Ts , Ms = self.Magnetizacion_(i)
            plt.plot(Ts, Ms, '^' , label = "L = {}".format(i))
        plt.xlabel("Temperatura")
        plt.ylabel("Magnetización")
        plt.title("Magnetización vs Temperatura")
        plt.legend()
        #guardamos las gráficas
        plt.savefig("Magnetizacion.png")
        
        plt.show()
        
    def Grafica_Calor_Especifico(self):
        """
        Esta función grafica el calor específico para diferentes longitudes de red.
        """
        for i in self.L:
            Ts , CVs = self.Calor_Especifico(i)
            plt.plot(Ts, CVs, '^' , label = "L = {}".format(i))
            
        #Trazamos un lina vertical en Tc
        plt.axvline(x=2.269, color='r', linestyle='--', label = "Tc")
        plt.xlabel("Temperatura")
        plt.ylabel("Calor Específico")
        plt.title("Calor Específico vs Temperatura")
        plt.legend()
        
        #guardamos las gráficas
        plt.savefig("Calor_Especifico.png")
        plt.show()

    def run(self):
        """
        ESte método ejecuta el programa para hacer manejo de errores.
        """
        if self.iteraciones <= 0:
            raise ValueError("iteraciones debe ser un entero mayor a cero")
        
        if not isinstance(self.L, list):
            raise TypeError("L debe ser una lista")
        
        if self.iteraciones <= 0:
            raise ValueError("iteraciones debe ser un entero mayor a cero")
        
        
        try:
            self.Grafica_Magnetizacion()
            self.Grafica_Calor_Especifico()
        except:
            print("Características inválidas")
             
        
# if __name__ == "__main__":
#     L= [2,4]
#     iterations = 1000
#     modelo = Modelo_Ising(L,iterations)
#     modelo.Grafica_Magnetizacion()
#     modelo.Grafica_Calor_Especifico()