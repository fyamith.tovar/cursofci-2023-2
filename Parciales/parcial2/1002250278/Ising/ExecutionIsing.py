from Ising import IsingCal
import matplotlib.pyplot as plt
import numpy as np
########################################################################################
# Profe, me va a perdonar este mierdero de Execution, pero ya tenia armada el ising.py #
########################################################################################
if __name__ == '__main__':
    V = []
    g = []
    m = []
    titles = ['L=2','L=4','L=6']
    L = np.array([2,4,6])
    for i in range(len(L)):
        ##############################################################################
        # El numero de iteraciones de MC es 1000 pa evitar overflow en normalizacion #
        ##############################################################################
        V.append(IsingCal(NumIter=1000, NumIterTemp=25, Ancho=L[i], Alto=L[i], Tempfinal=3.5))
        V[i].Run()
        g.append(V[i].CV)
        m.append(np.abs(V[i].Mag))
    
    plt.figure()
    for i in range(len(L)):
        plt.scatter(V[0].t, g[i], label =titles[i], marker='.')
    plt.title('Cv vs T')
    plt.xlabel('Temperatura')
    plt.ylabel('Cv')
    plt.legend()
    plt.savefig('CV.png')

    plt.figure()
    for i in range(len(L)):
        plt.scatter(V[0].t, m[i], label =titles[i], marker='.')
    plt.title('M vs T')
    plt.xlabel('Temperatura')
    plt.ylabel('M')
    plt.legend()
    plt.savefig('M.png')

    
