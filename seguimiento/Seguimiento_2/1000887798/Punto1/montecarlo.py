import numpy as np
import sympy as sp
import matplotlib.pyplot as plt
class Integral_montecarlo:

    """ Clase para calcular integrales por el metodo de montecarlo  """

    def __init__(self, f, a, b, n):
        
        """ Método constructor de la clase Integral_montecarlo
        :param f: Función a integrar en formato sympy
        :param a: Limite inferior de integración
        :param b: Limite superior de integración
        :param n: Número de interaciones  """

        self.f = f
        self.a = a
        self.b = b
        self.n = n
        x = sp.Symbol('x')
        self.f1 = sp.lambdify('x', self.f(x), 'numpy')
        self.integrada = sp.lambdify('x', self.f(x), "numpy")
        
        
    def x_monte(self,x):
            
            """ Método para generar los valores aleatorios en el intervalo [a,b]
            :param x: Número de iteraciones (Tamaño del intervalo)
            :return: Valores aleatorios en el intervalo [a,b] con tamaño x """
            try:
                xm = np.random.uniform(self.a, self.b, x)
            except:
                print("No se puede generar el vector de aleatorios")
            else:
                return xm        

    def area(self,x):

        """ Método para calcular el valor de la integral por el metodo de montecarlo 
        :param x: Número a evaluar en la función 
        :return: Valor de la integral por el método de montecarlo"""

        
        try:
            area = (self.b - self.a ) * np.mean(self.f1(self.x_monte(x)))
        except Exception as e:
            print("No se puede calcular el valor de la integral",e)
        else:
            return area
    
    def analitica(self):
        
        """ Método para calcular el valor de la integral por el metodo analítico"""
        try :
            x = sp.Symbol('x')
            f = sp.integrate(self.f(x), (x, self.a, self.b))
        except Exception as e:
            print("No se puede calcular el valor de la integral",e)
        else:
            return f
    
    
    def plot(self):
        
        """ Método para graficar la solución analítica y la 
        solución por el método de montecarlo 
        :return: Plot de la solución analítica vs la solución por
          el método de montecarlo """
        
        try:
            print('Graficando...')
            x = self.x_monte(self.n)
            x = np.sort(x)
            y_analitica   = []
            y_monte = []
            
            for i in x:
                y_monte.append(self.area(self.n))
                y_analitica.append(self.integrada(i))
        except Exception as e:
            print("No se pudo realizar el plot",e)
        else:
            plt.title('Integral por método de Montecarlo vs analítica')
            plt.plot(x,y_analitica, label = 'Analítica')
            plt.plot(x,y_monte,'.', label = 'Montecarlo')
            plt.grid()
            plt.legend()

            plt.savefig('montecarlo.png') 
            plt.close()
            print('Grafica guardada en montecarlo.png')
            
            return True
    
    def plot_iteraciones(self,n,N):

        """ Método para mirar como va convergiendo el método de montecarlo 
        :param n: paso para el número de iteraciones 
        :param N: número de iteraciones 
        :return: Plot de la convergencia del método de montecarlo"""

        try:
            print('Graficando...')
            
            monte = []
            analitica = []
            for i in range(1,N,n):
                monte.append(self.area(i))
                analitica.append(self.analitica())
        except Exception as e:
            print("No se pudo realizar el plot de montecarlo vs analitica",e)
        else:
            plt.title('Iteraciones del método de Montecarlo')
            plt.plot(monte, label = 'Montecarlo')
            plt.plot(analitica, label = 'Analítica')
            plt.legend()    
            plt.xlabel('Iteraciones')
            plt.ylabel('Valor de la integral')
            plt.grid()
            plt.savefig('iteracion.png')
            plt.close()
            print('Grafica guardada en iteracion.png')
            return True
        
