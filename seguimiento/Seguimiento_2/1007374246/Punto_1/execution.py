from integral import integracion
import sympy as sp
import numpy as np
if __name__ == "__main__":
    print("Bienvenido!")
    

    f = lambda x: sp.tanh(x/7)#x**2*sp.cos(x)
    a = 0   #limite inferior
    b = 1#sp.pi #limite superior
    n = 10000 #numero de iteraciones

    area = integracion(a, b,n,f)
    area.run()
    print("Resultado area montecarlo = {}".format(float(area.montecarlo(n))))
    print("Resultado con sympy = {}".format(float(area.analitica())))

