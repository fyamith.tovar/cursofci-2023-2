from integral import I_Montecarlo
import numpy as np

if __name__=="__main__":

    f=lambda x: np.tanh(x/7)#x**2 * np.cos(x)

    a=0
    b=1#np.pi
    n=1000

    solucion=I_Montecarlo(f,n,a,b)
    solucion.plot()
    solucion.run()
