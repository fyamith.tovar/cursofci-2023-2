# sol_edo.py
import matplotlib.pyplot as plt
import numpy as np


#Para llamar al Docstring de la clase
#print(edo.Sol_EDO.__doc__) 
#print(sol.__doc__) 

class Euler:
    """
    Metodo de euler para la solucion de EDO

    a y b: Intervalo de la solucion
      y_0: Condicion inicial
        f: Ecuacion Diferencial a solucionar
        N: Numero de divisiones en le intervalo
           Por defecto se toma N=1000
    """

    def __init__(self, a, b, y_0,f, N=1000):
        self.a = a
        self.b = b
        self.N = N
        self.funcion = f
        self.y_0 = y_0

    def solucion(self):
        dx = (self.b-self.a)/self.N
        self.x = [self.a + dx * i for i in range(self.N+1) ] # Intervalo X de la solucion
        
        
        self.y = np.zeros(len(self.x)) 
        self.y[0] = self.y_0 # Fijar la condicion inicial

        for i in range(self.N):
            # Calcular el valor de y para cada x
            self.y[i+1] = self.y[i] + dx*self.funcion(self.x[i],self.y[i])

        return [self.x, self.y]

    def grafica(self):
        
        plt.plot(self.x,self.y, label='Solucion Euler')
        #plt.show() # Se debe hacer desde el archivo main

    def interpolacion(self,x,x0,x1,y0,y1):
        # Esta funcion hace interpolacion lineal
        # x es el valor para el cual se desea saber su valor en y
        # x0,x1,y0,y1 son los valores conocidos para hacer la interpolacion
        return y0 + (y1 - y0)*(x - x0)/(x1 - x0)

    def intermedio(self,x):
        """
        Este metodo calcula un valor especifico de x dentro del intervalo de la solucion
        """
        if (x<self.a or x>self.b):
            print('El punto intermedio debe estar entre {} y {}'.format(self.a,self.b))
            return None
        # Primero se calcula entre que indices del array de los valores en x esta el valor que se desea calcular 
        indice = 0
        while x>self.x[indice]:
            indice += 1
        
        # Se definen los parametros para hacer la interpolacion
        x0 = self.x[indice - 1]
        x1 = self.x[indice]
        y0 = self.y[indice - 1]
        y1 = self.y[indice]

                # Se realiza la interpolacion 
        return self.interpolacion(x,x0,x1,y0,y1)


class RK4(Euler):
    """
    Esta clase es el metodo de RK4 que hereda todos los metodos del metodo de Euler exepto el de calcular la funcion
    """
    def __init__(self, a, b, y_0, f, N=1000):
        super().__init__(a, b, y_0, f, N)

    def solucion(self):
        dx = (self.b-self.a)/self.N
        self.x = [self.a + dx * i for i in range(self.N+1)] # Intervalo X de la solucion
        
        
        self.y = np.zeros(len(self.x)) 
        self.y[0] = self.y_0 # Fijar la condicion inicial

        for i in range(self.N):
            # Calcular el valor de y para cada x
            x_ = self.x[i]
            y_ = self.y[i]
            
            w1 = 1
            w2 = 2 
            w3 = 2
            w4 = 1

            k1 = self.funcion(x_,y_)
            k2 = self.funcion(x_+dx/2,y_+k1*dx/2)
            k3 = self.funcion(x_+dx/2,y_+k2*dx/2)
            k4 = self.funcion(x_+dx  ,y_+k3*dx)
            
            self.y[i+1] = self.y[i] + 1/6*dx*(w1*k1 + w2*k2 + w3*k3 + w4*k4)
            #self.y[i+1] = self.y[i] + w1*k1 + w2*k2 + w3*k3 + w4*k4
            #self.y[i+1] = self.y[i] + dx*self.funcion(self.x[i],self.y[i])
                # Original

        #self.y = self.y/1000
        return [self.x, self.y]
