import numpy as np
import matplotlib.pyplot as plt
import Lib_EDO as EDO

#NOTA 5
def f(x,y):
    # Ecuacion diferencial a resolver
    return 5*y/(x+1)

def sol_real(x):
    return (x+1)**5

if __name__ == "__main__":
    y_0 = 1
    a = 0
    b = 4
    punto_inermedio = 0.755*b # El punto debe de estar entre a y b

    x_real = np.linspace(a,b,1000)
    y_real = [sol_real(i) for i in x_real]
        # Valores de la solucion analitica



    sol_euler = EDO.Euler(a,b,y_0,f)
    # Solcuion de la EDO por el metodo de Euler 

    x_euler = sol_euler.solucion()[0]
    y_euler = sol_euler.solucion()[1]

    sol_punto_euler = sol_euler.intermedio(punto_inermedio)

    sol_RK = EDO.RK4(a,b,y_0,f)
    # Solcuion de la EDO por el metodo de RK4

    x_RK = sol_RK.solucion()[0]
    y_RK = sol_RK.solucion()[1]

    sol_punto_RK4 = sol_RK.intermedio(punto_inermedio)


    print('X final:', x_euler[-1])
    print("Solucion Euler en x = {} : {:.3f}".format(x_euler[-1],y_euler[-1]))
    print("Solucion   RK4 en x = {} : {:.3f}".format(x_RK[-1],y_RK[-1]))
    print("Solucion  real en x = {} : {:.3f}".format(x_real[-1],y_real[-1]))

    print('Solucion para el punto x = {} : {:.3f} '.format(punto_inermedio,sol_punto_RK4))

    # Grafica
    sol_euler.grafica() #plot de la solucion con Euler 
    sol_RK.grafica()    #plot de la solucion con RK4
    plt.plot(x_real, y_real,'--',label='Sol real') #plot de la solucion analitica

    if (sol_punto_euler !=None):
        plt.plot(punto_inermedio,sol_punto_euler,'b*',label="Solucion intermedia Euler")
    if (sol_punto_RK4 !=None):
        plt.plot(punto_inermedio,sol_punto_RK4,'r*',label="Solucion intermedia RK4")
    
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('Solucion de la EDO')
    plt.legend()
    plt.grid()
    plt.show()