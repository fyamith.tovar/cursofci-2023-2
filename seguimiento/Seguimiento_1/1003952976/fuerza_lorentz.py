import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

class campo_magnetico:

    '''Esta clase resuelve el problema de una partícula en un campo
       magnético uniforme y con la velocidad formando un ángulo
       theta con dicho campo. El campo apunta en la dirección z

       Los parámetros a introducir son los siguientes...(en el mismo 
       orden)
    
       Ek: energía cinética inicial de la particula.
           Por defecto se reciben valores de electronvoltios (eV)

       theta: ángulo que forma entre el campo magnético B
              y la velocidad de la partícula. Se reciben valores por defeto 
              en grados
              
        B: magnitud del campo magnético en la dirección z. las unidades están
           dadas en microteslas
           
        m: masa de la partícula. En Kilogramos (Kg)
        
        q: carga eléctrica del electrón. Coulombs (C)
        
        T: es un array que contiene dos elementos, el primero es el número
        de iteraciones que se quiere, y el segundo es el tamaño del paso'''

    def __init__(self,Ek,theta,B,m,q,T): #método constructor-asignación de atributos
        self.ek=Ek
        self.theta=theta
        self.B=B
        self.m=m
        self.q=q
        self.i=T[0] #iteraciones pedidas
        self.paso=T[1] #paso de las iteraciones

    def ajuste_unidades(self): 

        '''se deben ajustar los valores de las variables para
        que estén en el mismo sistema de unidades. En lo posible en el
        sistema internacional'''

        self.e_cinetica=(self.ek*1.6e-19) #se pasa a Julios
        self.B1=(self.B*1e-6) #microteslas a teslas
        self.theta1=(np.radians(self.theta))

        return [self.e_cinetica,self.B1,self.theta1]
    
    def W(self):
        '''frecuencia ciclotrónica en unidades correctas'''

        self.array=self.ajuste_unidades()
        self.w=(self.q*self.array[1])/self.m
        return self.w


    def solucionar_edo(self):
        '''Se prepara de manera ordenada el sistema de ecuaciones
        para poderlo ingresar de manera apropiada al algoritmo que lo
        soluciona.
        
        Vamos a resolver un sistema de ecuaciones diferenciales acopladas.
        El que sale del problema de la partícula cargada en un campo magnético 
        uniforme. Recordar que tal deducción sale de la fuerza de Lorentz
        
        este sitema es tal que:
        
        dVx/dt=(qB/m)Vy
        dVy/dt=-(qB/m)Vx
        
        para Vz la solución es lineal
        
        se tienen 4 ecuaciones:
        
        dX/dt=Vx
        dVx/dt=(qB/m)Vy
        dY/dt=Vy
        dVy/dt=-(qB/m)Vx
        '''

        
        def ecuaciones(t,condiciones): #función para resolver el sistema acoplado

            x,vx,y,vy=condiciones

            dxdt=vx
            dVxdt=self.W()*vy
            dydt=vy
            dVydt=(-1)*self.W()*vx

            return [dxdt,dVxdt,dydt,dVydt]  

        self.t=np.linspace(0,self.i*self.paso,self.i)

        self.unidades=self.ajuste_unidades()
        self.ci=np.array([0,(((2*self.unidades[0])/self.m)**(0.5) * \
            np.sin(self.unidades[2])),0,0])

        self.sol=solve_ivp(ecuaciones,np.array([0,self.i*self.paso]),self.ci,\
            t_eval=self.t)
        
        return self.sol


    def graficar(self):
       '''gráfica 3D'''

       self.sistema=self.ajuste_unidades()

       def Z(t):
        return (((2*self.sistema[0])/self.m)**(0.5) * \
            np.cos(self.sistema[2]))*t

       self.solucionar_edo()

       self.X=self.sol.y[0]
       self.Y=self.sol.y[2]
       self.tiempo=self.sol.t

       fig=plt.figure(figsize=(8,5))
       plt.style.use('dark_background')
       ax=fig.add_subplot(111,projection='3d')
       ax.plot(self.X,self.Y,Z(self.tiempo))
       ax.set_xlabel("X(m)")
       ax.set_ylabel("Y(m)")
       ax.set_zlabel("Z(m)")
       ax.set_title("trayectoria de la partícula en el campo")
       plt.savefig("trayectoria.png")


    def run(self):

        try:
            self.ajuste_unidades()
        
        except:
            print("ocurrió un error. Verifique los parámetros de entrada")
        
        try:
            self.W()
        except:
            print("verifique los valores de entrada. m=!0\
                Usted es inteligente, haga funcionar bien al pograma XD")
        
        try:
            self.solucionar_edo()
        except:
            print("Error. Revise las condiciones del problema")

        try:
            self.graficar()
        except:
            print("Ha ocurrido un error al graficar. \
                Intente cambiar los parámetros")




