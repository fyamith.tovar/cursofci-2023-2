import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
 
class ParticulalEnCampoMagnetico(object):
    """
    Esta clase calcula la trayectoria de una particula cargada en una region con campo magnetico
    
    Parametros de entrada:
        Ek: energia cinetica de la particula [eV]
        theta: angulo entre la velocidad y el campo magnetico [grados]
        phi: angulo entre la proyeccion de la velocidad en el plano XY y el eje X [grados]
        m: masa de la particula [kg]
        B: campo magnetico [microT]
        q: carga de la particula [C]
        x0: posicion inicial de la particula [x,y,z]
        t_final: tiempo final de simulacion [s]
        
    Parametros de salida:
        Trayectoria de la particula cargada en region sin campo magnetico y en region con campo magnetico
        (segun los parametros de entrada)
    """
    
    def __init__(self,Ek,theta,phi,m,B,q,x0,t_final):
        self.Ek=Ek*1.60217662e-19 
        self.theta=np.radians(theta)
        self.phi=phi
        self.m=m
        self.B=B*1e-6
        self.q=q
        self.t_final=t_final
        self.t = np.linspace(0, self.t_final, 10000)
        
        self.x0=x0
        self.v0 = [np.sqrt(2*self.Ek/self.m)*np.sin(self.theta)*np.cos(self.phi),\
                   np.sqrt(2*self.Ek/self.m)*np.sin(self.theta)*np.sin(self.phi),\
                   np.sqrt(2*self.Ek/self.m)*np.cos(self.theta)]
        
    #Antes de ingresar a la region con campo magnetico
    def FreeMotion(self):
        x = self.x0[0] + self.v0[0]*self.t
        y = self.x0[1] + self.v0[1]*self.t
        z = self.x0[2] + self.v0[2]*self.t
        return x,y,z
    
    #Punto de entrada a la region con campo magnetico
    def PuntoEntrada(self):
        momento_entrada = None
        
        for i in range(len(self.t)):
            if self.FreeMotion()[1][i] >= 0:
                momento_entrada = [self.FreeMotion()[0][i],
                                   self.FreeMotion()[1][i],
                                   self.FreeMotion()[2][i]]
                break
        return momento_entrada
    
    #Ecuacion de movimiento de la particula cargada en un campo magnetico
    def EcuacionMovimiento(self,y,t):
        x,x_dot,y,y_dot,z,z_dot = y
         
        x_ddot =  ((self.q*self.B)/self.m)*y_dot
        y_ddot = -((self.q*self.B)/self.m)*x_dot
        z_ddot = 0
        
        return [x_dot,x_ddot,y_dot,y_ddot,z_dot,z_ddot]
        
    #Trayectoria de la particula cargada en un campo magnetico 
    def Trayectoria(self):
        y0 = [self.PuntoEntrada()[0],self.v0[0],\
              self.PuntoEntrada()[1],self.v0[1],\
              self.PuntoEntrada()[2],self.v0[2] ]
        
        return odeint(self.EcuacionMovimiento, y0, self.t)
    
    #Filtrado de la trayectoria de la particula dentro de la region con campo magnetico
    def filtrado(self):
        indice_negativo = np.where(self.Trayectoria()[:, 2] < 0)[0]
        
        if len(indice_negativo) == 0:
            return self.Trayectoria()
    
        primer_indice_negativo = indice_negativo[0]
        return self.Trayectoria()[:primer_indice_negativo, :]

    #garfica la trayectoria de la particula
    def PlotTrayectoria(self):
        sol = self.filtrado()
        
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(sol[:, 0], sol[:, 2], sol[:, 4],'k', label='Trayectoria en el campo magnetico')
        ax.plot([self.FreeMotion()[0][0],self.PuntoEntrada()[0]],\
                [self.FreeMotion()[1][0],self.PuntoEntrada()[1]],\
                [self.FreeMotion()[2][0],self.PuntoEntrada()[2]], 'r',label='Trayectoria libre')

        #plano que separa la region con campo magnetico de la region sin campo magnetico
        x = np.linspace(sol[:, 0].min(), sol[:, 0].max(), 100)
        z = np.linspace(sol[:, 4].min(), sol[:, 4].max(), 100)
        X, Z = np.meshgrid(x, z)
        Y = 0*X
        ax.plot_surface(X, Y, Z, alpha=0.5)

        ax.set_title('Trayectoria de una particula cargada en una region con campo magnetico')
        ax.set_xlabel('x [m]')
        ax.set_ylabel('y [m]')
        ax.set_zlabel('z [m]')
        ax.view_init(30, -40)
        plt.legend()
        plt.savefig('Trayectoria.png')
        plt.show()

    #nos aseguramos de que todo el codigo se ejecute correctamente
    def run(self):
        if self.x0[1] > 1e-4:
            print("La particula debe estar fuera de la region con campo magnetico")
            return False
        elif self.theta < 0 or self.theta > 180:
            print("El angulo theta debe estar entre 0 y 180 grados")
            return False
        elif self.phi < 0 or self.phi > 180:
            print("El angulo phi debe estar entre 0 y 180 grados")
            return False
        else:
    
            try:
                #self.FreeMotion()
                #self.PuntoEntrada()
                #self.Trayectoria()
                #self.filtrado()
                self.PlotTrayectoria()
            except:
                print(TypeError("Error en los argumentos"))
                return False
            return True
    
        
    
        
