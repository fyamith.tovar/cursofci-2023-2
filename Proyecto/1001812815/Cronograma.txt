Cronograma

Semana 1 (23-27 octubre)
- Complementar el Estado del Arte
- Investigar y seleccionar las herramientas necesarias para la elaboración del proyecto
- Realizar una capacitación teórica sobre las herramientas seleccionadas
- Opcional: Estructuración del código basado en objetos

Semana 2 (30 octubre - 03 noviembre)
- Estructuración del código basado en objetos (si no se realizó en la Semana 1)
- Creación del repositorio en la plataforma elegida (por ejemplo, GitHub)
- Establecer una estructura clara en el repositorio, incluyendo carpetas para documentación, código fuente, pruebas, etc.
- Comenzar la construcción del código (Parte 1 de 2)

Semana 3 (06-10 noviembre)
- Continuar con la construcción del código (Parte 2 de 2)
- Realizar pruebas de compilación para asegurar el correcto funcionamiento del código
- Implementar un sistema de manejo de errores para identificar y corregir posibles fallos

Semana 4 (13-17 noviembre)

- Validar y estabilizar el programa, asegurando que cumple con los requisitos del proyecto
- Mejorar el sistema de manejo de errores, si es necesario, para garantizar la robustez del programa
- Documentar el código de manera detallada, incluyendo comentarios en el código y elaborando una documentación completa que explique el funcionamiento del programa y cómo utilizarlo

