# dataton2023-Mycelium
Repositorio para la solución del reto de la Dataton 2023 del grupo Mycelium.

# Horario de empleados

Código en Python para generar un horario optimizado para los empleados de una sucursal de Bancolombia, teniendo en cuenta la demanda de empleados y las siguientes condiciones:

1. Cada 1-2 horas todos los empleados deben tener una pausa activa.
2. Todos deben tener 1.5 horas de almuerzo que inician entre las 11:30 y 1:30.
3. Cada franja de trabajo debe ser de mínimo 1 hora.
4. En todas las franjas horarias debe haber un mínimo de 1 empleado.
5. Las jornadas de cada uno de los empleados son de 8 horas continuas.

## Instalación

Clona el repositorio a tu máquina local.

```bash
git clone https://github.com/jfjm512/dataton2023-Mycelium.git
```

Navega el repositorio clonado.

```bash
cd nombre_repositorio
```
Aquí encontrarás dos carpetas (docs y src) y con el README.md.

Si deseas acceder a la documentación del código, ingresa a la carpeta de docs. Aquí lo encontrarás:
```bash
 cd .\docs\
```

Si lo que quieres es correr el código, ingresa a src:
```bash
cd .\src\
```
En src podrás encontrar el archivo execution.py, que es el desarrollo del proyecto, Dataton 2023 Etapa2.xlsx que son los datos con los que se genera el archivo Horario_final.csv, el cual estará en el formato que acepta la página https://dataton.cde-aia-bc.net/

También encontramos los requirements.txt, los cuales son las librerías necesarias para correr el código. Esto se descarga de la siguiente manera:

```bash
pip install -r .\requirements.txt
```

Además encontramos la carpeta Módulos, en la cual se encuentran archivos necesarios para correr el execution.py. Tenemos modulo1.py, modulo2.py y modulo3.py.

## Uso
Si queremos generar un horario, nos ubicamos en la carpeta src y corremos el siguiente comando en la terminal:

```bash
python .\execution.py
```

E ingrese la ruta al documento cuando el programa se lo pida.

Esto creará los horarios en el archivo Horario_final.csv
