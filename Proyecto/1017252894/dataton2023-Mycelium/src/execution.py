#Importa los modulos de python
import pandas as pd
import numpy as np

#Importa los modulos creados
import Modulos.modulo2 as H
import Modulos.modulo3 as m3

if __name__ == '__main__':
    
    #Carga los datos
    try:
        data = str(input("Ingrese la ruta del archivo: "))
    except:
        print("Error al cargar el archivo, por favor verifique la ruta")
        exit()        
    
    demand = pd.read_excel(data, sheet_name="demand")
    workers = pd.read_excel(data, sheet_name="workers")
    
    #Extrae la información de los trabajadores
    workers_info = m3.Workers(demand,workers).Workers()
    
    #Se extraen las sucursales
    sucursales = demand["suc_cod"].unique()

    #Creamos un horario a partir de la clase Horario
    Hor = H.Horario()
    
    #Hor.tiempo_opti = 5
    #Hor.total_optimizaciones = 3    
    horario_final = []
    
    for act in range(len(sucursales)):
        nombre_sucursal = str(sucursales[act])
        sucursal = demand[demand['suc_cod'] == sucursales[act]].reset_index(drop=True)
        trabajadores = workers[workers['suc_cod']==sucursales[act]].reset_index(drop=True)

        franjas = 49 # + 29
        empleados = len(trabajadores)

        horario = np.zeros((franjas*5+29 +5,empleados+3))
        # El numero de filas es el total de franjas por dia 
        # multiplicado por los días de la semana (5)
        # mas las franjas de los sábados

        # El numero de columnas es el total de empleados 
        # mas una columna para el total de empleados disponibles en cada franja  
        # otra columna para la demanda 
        # y la ultima columna es el balance entre disponibles y demanda

        horario[5:,empleados+1] = sucursal["demanda"]
        # Fijar la demanda 

        # Determinar el tipo de contrato para cada empleado
        # La segunda fila de la matriz hace referencia al tipo de contrato
        # 8 == TC
        # 4 == MT
        for i in range(empleados):
            
            horario[0][i] = trabajadores['documento'][i]
            if (trabajadores['contrato'][i] == "TC"):
                horario[1][i] = 8

            elif (trabajadores['contrato'][i] == "MT"):
                horario[1][i] = 4
            else:
                print("Error al fijar el tipo de contrato")

        # Calcular el horario y su puntaje
        horario = Hor.CalcularHorario(horario,workers_info[act][3],workers_info[act][4])
        puntaje = Hor._CalcularPuntaje(horario)
        
        
        if Hor._Correcto(horario):
            horario_final.append(m3.Workers(demand,workers,horario,int(nombre_sucursal)).EntregaFinal())
            print("Sucursal: {} - {}".format(nombre_sucursal,int(puntaje)))
        else:
            print("Error")

    #Junta todos los horarios en uno solo
    horario_final = pd.concat(horario_final)
    
    #Ordena las columnas de la forma correcta
    horario_final = horario_final[['suc_cod','documento','fecha','hora','estado','hora_franja']]
    
    #Guarda el horario final en un archivo csv
    horario_final.to_csv("Horario_final.csv", index=False)
        